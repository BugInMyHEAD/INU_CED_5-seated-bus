' EV3 Basic source code for the project, INU_CED_5-seated-bus' FinalExam

' Copyright 2017. BugInMyHEAD all rights reserved.

' References
' https://sites.google.com/site/ev3basic/


' Naming convention
' /[a-z][A-z0-9_]*/ global variable (using as a constant is recommended)
' /[A-Z][A-z0-9_]*/ subroutine
' /x__{Subroutine}__[a-z][A-z0-9_]*/ local(extra) variable
' /{Subroutine}__p[A-z0-9_]+/ parameter
' /{Subroutine}__r[A-z0-9_]*/ return value


' Variables for convenient coding, DON'T CHANGE THEM
' In EV3, "true" is not equal to "True"
ev3true = "True"
ev3false = "False"
true = ev3true
false = ev3false

' Variables related with sensor types
typeColorSensor = 29

' Variables related with modes
colorSensorReflect = 0
colorSensorAmbient = 1
colorSensorColor = 2
colorSensorRaw = 4

' Variables related with ports
colorSensorPort = -1 ' A negative value for automatic initializing, normal value in 1~4
leftMotorPort = "B"
rightMotorPort = "C"
bothMotorPorts = leftMotorPort + rightMotorPort

' Variables related with moving distance
' ATTENTION: motor commands will always use the absolute value of the degree parameter.
motorDegBtwnAxleAndSensor = 250
motorDegMesureLight = 50

' Variables related with speed and steering
directionOffset = 0 ' -100~100
straight = 0
cw = 100 ' -100 or 100
ccw = -cw
right = cw / 2
left = -right
fast = 70 ' -100 ~ 100
slow = fast ' Recommended to be slower than 'fast'
lightMedianIntensity = -1 ' A negative value for automatic initializing, normal value in 0~100
lightLowIntensity = -1 ' A negative value for automatic initializing, normal value in 0~100
lightHighIntensity = -1 ' A negative value for automatic initializing, normal value in 0~100
lightThresholdOffset = 0 ' 0~100

' Variables related with LCD screen
white = 0
black = 1
small = 0
medium = 1
large = 2

' Additional variables
delayTime = 200 ' miliseconds
noteVolume = 0 ' 0~100
noteDuration = 200 ' miliseconds
playVolume = 100

' Parameters and return values
RefreshScreen__p1 = ""
RefreshScreen__p2 = ""
RefreshScreen__p3 = ""
RefreshScreen__p4 = ""
RefreshScreen__p5 = ""
BackToCourse__pTurnToCourse = straight ' -100~100
BackToCourse__pMotorDeg = motorDegBtwnAxleAndSensor
BackToCourse__pLazyStop = false
BackToCourseHelper__pTurnToCourse = straight
BackToCourseHelper__pMotorPort = leftMotorPort
Drive__pTurnWhileAdjust = straight ' -100~100, when the color sensor is on the bright side
Drive__pMotorDegToAdjust = motorDegBtwnAxleAndSensor
Drive__pTurnUntilCourseOut = straight ' -100~100, when the color sensor is on the bright side
Drive__pMotorDegCourseEnd = motorDegBtwnAxleAndSensor
Drive__pMotorDegCourseOut = motorDegBtwnAxleAndSensor


' Main

Motor.Invert(bothMotorPorts)

' Starting point is on the lower left corner of the shape of a house
x__phase = 0
RefreshScreenClearParams()
RefreshScreen__p1 = "Phase " + x__phase
RefreshScreen()

' Getting light intensity of the dark side and the bright side
If colorSensorPort < 0 Then
  For i1 = 1 To 4
    If Sensor.GetType(i1) = typeColorSensor Then
      colorSensorPort = i1
    EndIf
  EndFor
EndIf

Sensor.SetMode(colorSensorPort, colorSensorReflect)

If lightMedianIntensity < 0 Then
  x__darkest = 100
  x__brightest = 0
  Motor.MoveSteer(bothMotorPorts, slow, cw, motorDegMesureLight, ev3true)
  Program.Delay(delayTime)
  
  Motor.ScheduleSteer(bothMotorPorts, slow, ccw, 2 * motorDegMesureLight, ev3true)
  While Motor.IsBusy(bothMotorPorts)
    x__light = Sensor.ReadPercent(colorSensorPort)
    If x__light < x__darkest Then
      x__darkest = x__light
    ElseIf x__brightest < x__light Then
      x__brightest = x__light
    EndIf
  EndWhile
  Program.Delay(delayTime)
  
  Motor.MoveSteer(bothMotorPorts, slow, cw, motorDegMesureLight, ev3true)
  Program.Delay(delayTime)
  lightMedianIntensity = ( x__darkest + x__brightest ) / 2
EndIf

If lightLowIntensity < 0 Then
  lightLowIntensity = lightMedianIntensity - lightThresholdOffset
EndIf
If lightHighIntensity < 0 Then
  lightHighIntensity = lightMedianIntensity + lightThresholdOffset
EndIf

x__turnWhileAdjust = 13 ' -100~100, when the color sensor is on the bright side if the left motor port is B
x__turnUntilCourseOut = 5 ' -100~100, when the color sensor is on the bright side if the left motor port is B
x__motorDegCourseOut = 360

x__startingPhase = 1 ' 1~8

' Driving |
x__phase = 1
If x__startingPhase <= x__phase Then
  RefreshScreen__p1 = "Phase " + x__phase
  RefreshScreen()
  Speaker.Play(playVolume, "scv_good_to_go_sir")
  Drive__pTurnWhileAdjust = x__turnWhileAdjust
  Drive__pMotorDegToAdjust = 700
  Drive__pTurnUntilCourseOut = x__turnUntilCourseOut
  Drive__pMotorDegCourseEnd = 1900
  Drive__pMotorDegCourseOut = x__motorDegCourseOut
  Drive()
  Motor.MoveSteer(bothMotorPorts, -fast, left, 165, ev3true)
  Program.Delay(delayTime)
EndIf

' Driving /
x__phase = 2
If x__startingPhase <= x__phase Then
  RefreshScreen__p1 = "Phase " + x__phase
  RefreshScreen()
  Drive__pTurnWhileAdjust = x__turnWhileAdjust
  Drive__pMotorDegToAdjust = 700
  Drive__pTurnUntilCourseOut = x__turnUntilCourseOut
  Drive__pMotorDegCourseEnd = 1600
  Drive__pMotorDegCourseOut = x__motorDegCourseOut
  Drive()
  Motor.MoveSteer(bothMotorPorts, -fast, left, 395, ev3true)
  Program.Delay(delayTime)
EndIf

' Driving \
x__phase = 3
If x__startingPhase <= x__phase Then
  RefreshScreen__p1 = "Phase " + x__phase
  RefreshScreen()
  Drive__pTurnWhileAdjust = x__turnWhileAdjust
  Drive__pMotorDegToAdjust = 900
  Drive__pTurnUntilCourseOut = x__turnUntilCourseOut
  Drive__pMotorDegCourseEnd = 1600
  Drive__pMotorDegCourseOut = x__motorDegCourseOut
  Drive()
  Motor.MoveSteer(bothMotorPorts, -fast, left, 155, ev3true)
  Program.Delay(delayTime)
EndIf

' Driving |
x__phase = 4
If x__startingPhase <= x__phase Then
  RefreshScreen__p1 = "Phase " + x__phase
  RefreshScreen()
  Drive__pTurnWhileAdjust = x__turnWhileAdjust
  Drive__pMotorDegToAdjust = 700
  Drive__pTurnUntilCourseOut = x__turnUntilCourseOut
  Drive__pMotorDegCourseEnd = 2200
  Drive__pMotorDegCourseOut = x__motorDegCourseOut
  Drive()
  Motor.MoveSteer(bothMotorPorts, -fast, left, 335, ev3true)
  Program.Delay(delayTime)
EndIf

' Driving -
x__phase = 5
If x__startingPhase <= x__phase Then
  RefreshScreen__p1 = "Phase " + x__phase
  RefreshScreen()
  Drive__pTurnWhileAdjust = x__turnWhileAdjust
  Drive__pMotorDegToAdjust = 1100
  Drive__pTurnUntilCourseOut = x__turnUntilCourseOut
  Drive__pMotorDegCourseEnd = 2200
  Drive__pMotorDegCourseOut = x__motorDegCourseOut
  Drive()
  Motor.MoveSteer(bothMotorPorts, fast, cw, 290, ev3true)
  Program.Delay(delayTime)
EndIf

' Driving /
x__phase = 6
If x__startingPhase <= x__phase Then
  RefreshScreen__p1 = "Phase " + x__phase
  RefreshScreen()
  Speaker.Play(playVolume, "right_way_sir")
  Drive__pTurnWhileAdjust = x__turnWhileAdjust
  Drive__pMotorDegToAdjust = 700
  Drive__pTurnUntilCourseOut = x__turnUntilCourseOut
  Drive__pMotorDegCourseEnd = 2700
  Drive__pMotorDegCourseOut = x__motorDegCourseOut
  Drive()
  Motor.MoveSteer(bothMotorPorts, -fast, right, 520, ev3true)
  Program.Delay(delayTime)
EndIf

' Driving -
x__phase = 7
If x__startingPhase <= x__phase Then
  RefreshScreen__p1 = "Phase " + x__phase
  RefreshScreen()
  Drive__pTurnWhileAdjust = -x__turnWhileAdjust
  Drive__pMotorDegToAdjust = 900
  Drive__pTurnUntilCourseOut = -x__turnUntilCourseOut
  Drive__pMotorDegCourseEnd = 2200
  Drive__pMotorDegCourseOut = x__motorDegCourseOut
  Drive()
  Motor.MoveSteer(bothMotorPorts, fast, ccw, 295, ev3true)
  Program.Delay(delayTime)
EndIf

' Driving \
x__phase = 8
If x__startingPhase <= x__phase Then
  RefreshScreen__p1 = "Phase " + x__phase
  RefreshScreen()
  Drive__pTurnWhileAdjust = -x__turnWhileAdjust
  Drive__pMotorDegToAdjust = 700
  Drive__pTurnUntilCourseOut = -x__turnUntilCourseOut
  Drive__pMotorDegCourseEnd = 2500
  Drive__pMotorDegCourseOut = x__motorDegCourseOut
  Drive()
EndIf

' Let's party!
Motor.Stop(bothMotorPorts, ev3true)
Sensor.SetMode(colorSensorPort, colorSensorRaw)
RefreshScreen__p1 = "End"
RefreshScreen__p2 = "" + EV3.Time
RefreshScreen()
Speaker.Play(playVolume, "job_s_finished")
Program.Delay(30000)

' End of Main


' Subroutines

Sub RefreshScreen
  LCD.StopUpdate()
  LCD.Clear()
  x__RefreshScreen__lineHeight = 25
  LCD.Text(black, 0, 0 * x__RefreshScreen__lineHeight, large, RefreshScreen__p1)
  LCD.Text(black, 0, 1 * x__RefreshScreen__lineHeight, large, RefreshScreen__p2)
  LCD.Text(black, 0, 2 * x__RefreshScreen__lineHeight, large, RefreshScreen__p3)
  LCD.Text(black, 0, 3 * x__RefreshScreen__lineHeight, large, RefreshScreen__p4)
  LCD.Text(black, 0, 4 * x__RefreshScreen__lineHeight, large, RefreshScreen__p5)
  LCD.Update()
EndSub

Sub RefreshScreenClearParams
  RefreshScreen__p1 = ""
  RefreshScreen__p2 = ""
  RefreshScreen__p3 = ""
  RefreshScreen__p4 = ""
  RefreshScreen__p5 = ""
EndSub

Sub Drive
  x__Drive__turnToBorder = Drive__pTurnWhileAdjust
  x__Drive__turn = straight
  x__Drive__speed = slow
  x__Drive__endOfCourse = false
  x__Drive__twiceMotorDegToAdjust = 2 * Drive__pMotorDegToAdjust
  x__Drive__twiceMotorDegCourseEnd = 2 * Drive__pMotorDegCourseEnd
  x__Drive__twiceMotorDegCourseOut = 2 * Drive__pMotorDegCourseOut
  
  Motor.ResetCount(bothMotorPorts)
  While true
    ' Steering with measuring light intensity
    x__Drive__lightIntensity = Sensor.ReadPercent(colorSensorPort)
    If x__Drive__lightIntensity > lightHighIntensity Then
      x__Drive__turn = x__Drive__turnToBorder
    Else
      If x__Drive__lightIntensity < lightLowIntensity Then
        x__Drive__turn = -x__Drive__turnToBorder
      Else
        x__Drive__turn = straight
      EndIf
      If x__Drive__endOfCourse Then
        Motor.ResetCount(bothMotorPorts)
      EndIf
    EndIf
    
    x__Drive__count = Math.Abs(Motor.GetCount(leftMotorPort)) + Math.Abs(Motor.GetCount(rightMotorPort))
    x__Drive__speed = fast
    If x__Drive__count >= x__Drive__twiceMotorDegToAdjust Then
      x__Drive__turnToBorder = Drive__pTurnUntilCourseOut
    EndIf
    If x__Drive__endOfCourse = false And x__Drive__count >= x__Drive__twiceMotorDegCourseEnd Then
      x__Drive__endOfCourse = true
      Motor.ResetCount(bothMotorPorts)
    ElseIf x__Drive__endOfCourse And x__Drive__count >= x__Drive__twiceMotorDegCourseOut Then
      Goto __exit_drive
    EndIf
    RefreshScreen__p3 = x__Drive__count + ""
    RefreshScreen()
    
    Motor.StartSteer(bothMotorPorts, x__Drive__speed, x__Drive__turn + directionOffset)
  EndWhile
  
  __exit_drive:
  Motor.Stop(bothMotorPorts, ev3true)
  Program.Delay(delayTime)
EndSub

Sub BackToCourseHelper
  Motor.StartSteer(bothMotorPorts, slow, BackToCourseHelper__pTurnToCourse + directionOffset)
  RefreshScreen__p3 = "" + Math.Abs(Motor.GetCount(BackToCourseHelper__pMotorPort))
  RefreshScreen()
EndSub

Sub BackToCourse ' not used but let it be.
  If Math.Abs(cw + BackToCourse__pTurnToCourse) < Math.Abs(cw) Then
    x__BackToCourse__motorPort = rightMotorPort
  Else
    x__BackToCourse__motorPort = leftMotorPort
  EndIf
  
  Motor.ResetCount(bothMotorPorts)
  Motor.MoveSteer(bothMotorPorts, fast, BackToCourse__pTurnToCourse + directionOffset, BackToCourse__pMotorDeg, ev3true)
  While Sensor.ReadPercent(colorSensorPort) > lightHighIntensity
    BackToCourseHelper__pTurnToCourse = BackToCourse__pTurnToCourse
    BackToCourseHelper__pMotorPort = x__BackToCourse__motorPort
    BackToCourseHelper()
  EndWhile
  If BackToCourse__pLazyStop Then
    While Sensor.ReadPercent(colorSensorPort) >= lightLowIntensity
      BackToCourseHelper__pTurnToCourse = BackToCourse__pTurnToCourse
      BackToCourseHelper__pMotorPort = x__BackToCourse__motorPort
      BackToCourseHelper()
    EndWhile
    While Sensor.ReadPercent(colorSensorPort) < lightLowIntensity
      BackToCourseHelper__pTurnToCourse = BackToCourse__pTurnToCourse
      BackToCourseHelper__pMotorPort = x__BackToCourse__motorPort
      BackToCourseHelper()
    EndWhile
  EndIf
  Motor.Stop(bothMotorPorts, ev3true)
  Program.Delay(delayTime)
EndSub